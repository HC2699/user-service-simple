package deploy;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "/user")
public class Controller {

    @Autowired
    EmployeeDTO employeeDTO;

    @GetMapping
    public String check(){
        return "Welcome!";
    }

    @PostMapping(path = "/postuser")
    public String addEmployee(@RequestParam int id,@RequestParam String name,@RequestParam String email){
        Employee employee = new Employee(id,name,email);
        int done = employeeDTO.addEmployee(employee);
        if (done == 1) return "Successful!";
        return "Fail!";
    }

    @GetMapping(path = "/getUsername")
    public List<String> getUsername(){
        return employeeDTO.getName();
    }

    @GetMapping(path = "/getAll")
    public List<Employee> getAll(){
        return employeeDTO.getAll();
    }

    @GetMapping(path = "/getById")
    public Employee getEmployeeById(@RequestParam int id){
        return employeeDTO.getEmployeeById(id);
    }

    @DeleteMapping(path = "/deleteById")
    public String deleteById(@RequestParam int id){
        int message =  employeeDTO.deleteById(id);
        if (message == 1) return "Successful!";
        return "Fail!";
    }
}
