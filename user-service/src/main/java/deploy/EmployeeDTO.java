package deploy;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class EmployeeDTO {
    @Autowired
    JdbcTemplate jdbcTemplate;

    public int addEmployee(Employee employee){
        String query = "INSERT employee VALUES(?, ?, ?)";
        return jdbcTemplate.update(query, employee.getId(), employee.getName(), employee.getEmail());
    }

    public List<String> getName(){
        List<String> listName = new ArrayList<String>();
        String query = "SELECT name FROM employee;";
        listName.addAll(jdbcTemplate.queryForList(query, String.class));
        return listName;
    }

    public List<Employee> getAll(){
        List<Employee> employeeList = new ArrayList<>();
        String query = "SELECT * FROM employee;";
        employeeList = jdbcTemplate.query(query, new EmployeeRowMapper());
        return employeeList;
    }

    public Employee getEmployeeById(int id){
        String query = "SELECT * FROM employee WHERE id = ?";
        Employee employee = jdbcTemplate.queryForObject(query,new Object[] { id }, new EmployeeRowMapper());
        return employee;
    }

    public int deleteById(int id){
        String query = "DELETE FROM employee WHERE id = ?";
        return jdbcTemplate.update(query, id);
    }
}
