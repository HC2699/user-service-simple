# switch to user-service module
cd user-service/

# build jar file
mvn clean package

# build image with jar file and dependency
docker image build -t user-service-app:0.1 .

# switch to nginx module
cd .. && cd nginx

# build image nginx
docker image build -t custom-nginx-01 .
